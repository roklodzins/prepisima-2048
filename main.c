  // Copyright © 2021 - Romain Klodzinski & Evahn Le Gal
 //  2048 Game - SDL 1.2 Version
//   Crossplatform: Windows (10 et 11) + Linux testé et garantie


// Pragma
#pragma GCC optimize("Ofast") // force gcc à activer toutes les optimisations


// Includes
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "SDL.h"
#include "SDL_types.h"
#include "tileset.c" // version hardcodée de tileset.png, les pixels RBGA sont a l'état bruts et injectable dans une SDL_Surface

#ifdef _WIN32
#include <direct.h>
#define chdir _chdir
#else
#include <unistd.h>
#endif


// Typedef
typedef unsigned int  uint32;
typedef unsigned char uint8;

typedef char  int8;
typedef _Bool bool; // un enum marcherait aussi mais serait moins pratique pour certaine optimisation



// Struct
typedef struct MoveData {
	int8 vectorX: 3, vectorY: 3;
	bool mergedTarget: 1, moving: 1;
} MoveData;


// Enum
typedef enum Move {
	Right = 0u,
	Up = 1u,
	Left = 2u,
	Down = 3u,
	Save = 4u,
	Load = 5u,
	None = 6u,
	New = 7u,
} Move; 


// Macros

// Dimentions en pixels
#define FramePadding 128 // espacement autour du plateau
#define HudPadding 32   //  espacement dans le HUD
#define Hud 128        //   hauteur du HUD / de l'interface (le bandeau en haut)
#define Padding 16    //    espacement entre les cellules du plateau
#define TileTx 12    //     largueur des tuiles textuelles (les scores)
#define TileTy 24   //      hauteur des tuiles textuelles
#define TileEy 64  //       hauteur de la tuile "partie terminée"
#define TileS 96  //        taille des tuiles carrées (les nombres)

#define WIDTH  ((TileS << 2) + Padding * 5 + (FramePadding << 1))
#define HEIGHT ((TileS << 2) + Padding * 5 + (FramePadding << 1) + Hud)


// Durées en millisec
#define AnimationFlash 5 // durée d'une frame de fondu transparent
#define AnimationMove 5 // durée d'une frame de déplacement


// Autres Macros
#define cellPos(pos, i) pos.x = Padding * (1 + i % 4) + TileS * (i % 4) + FramePadding, pos.y = Padding * (1 + i / 4) + TileS * (i / 4) + FramePadding + Hud;
#define NewTile(i) {TileS * (i % 4), TileS * (i / 4), TileS, TileS}
#define NewNumTile(i) {TileTx * i, TileS * 4, TileTx, TileTy}


// Global variables

SDL_Rect TileSector[] = {NewTile(14), NewTile(0),  NewTile(1),  NewTile(2),    // 0,    2,    4,    8
						 NewTile(3),  NewTile(4),  NewTile(5),  NewTile(6),   // 16,   32,   64,  128
						 NewTile(7),  NewTile(8),  NewTile(9),  NewTile(10), // 256,  512, 1024, 2048
						 NewTile(11), NewTile(12), NewTile(13)}; 			// 4096, 8192, inf

SDL_Rect TextSector[] = {{TileS * 3, TileS * 3, TileS, TileTy}, {TileS * 3, TileS * 3 + TileTy, TileS, TileTy}, // "Meilleur" / "Score"
						 {TileTx * 10 + 5, TileS << 2, TileS * 4 - TileTx * 10 - 5, TileEy + (TileTy << 1)},   // Info commande
						 {0, (TileS << 2) + TileTy, TileTx * 10 + 5, TileEy},            // "Partie Terminée"
						 {0, (TileS << 2) + TileTy + TileEy, TileTx * 10 + 5, TileTy}}; // "Victoire"

SDL_Rect NumberSector[] = {NewNumTile(0), NewNumTile(1), NewNumTile(2), NewNumTile(3), NewNumTile(4),
						   NewNumTile(5), NewNumTile(6), NewNumTile(7), NewNumTile(8), NewNumTile(9)};


SDL_Surface * Screen, * TileSet; // écran et tileset (voir tileset.c / tileset.png)

uint8 GameField[16]; // on stock ici les exposants des nombres et non leur valeur
uint32 Score = 0, HighScore = 0;



void BlitScore() { // affiche les scores et les commandes

	SDL_Rect pos = {0, 0, WIDTH, Hud};
	SDL_FillRect(Screen, &pos, SDL_MapRGB(Screen->format, 51, 51, 51));

	pos.x = HudPadding, pos.y = 9, pos.w = TextSector[2].w, pos.h = TextSector[2].h;

	SDL_BlitSurface(TileSet, &TextSector[2], Screen, &pos); // affiche les infos de commandes

	char s[11];
	
	sprintf(s, "%u", Score); // convertie le score en string

	pos.x = (HudPadding << 1) + (TileTx << 1) + TextSector[2].w, pos.y = (HudPadding << 1) - TileTy - (TileTy >> 2), pos.w = TextSector[1].w, pos.h = TextSector[1].h;
	SDL_BlitSurface(TileSet, &TextSector[1], Screen, &pos); // affiche "Score"

	pos.x += TextSector[0].w + pos.w + (TileTx + (TileTx >> 2)) * 9, pos.w = TileTx;

	for (uint8 i = strlen(s) - 1; i < (uint8)-1; i--) {
		pos.x -= pos.w + (TileTx >> 2);
		SDL_BlitSurface(TileSet, &NumberSector[s[i] - '0'], Screen, &pos); // affiche les chiffres
	}


	sprintf(s, "%u", HighScore); // convertie le score en string

	pos.x = (HudPadding << 1) + (TileTx << 1) + TextSector[2].w, pos.y = (HudPadding << 1) + (TileTy >> 2), pos.w = TextSector[0].w;
	SDL_BlitSurface(TileSet, &TextSector[0], Screen, &pos); // affiche "Meilleur"

	pos.x += TextSector[0].w, pos.w = TextSector[1].w;
	SDL_BlitSurface(TileSet, &TextSector[1], Screen, &pos); // affiche "Score"

	pos.x += pos.w + (TileTx + (TileTx >> 2)) * 9, pos.w = TileTx;

	for (uint8 i = strlen(s) - 1; i < (uint8)-1; i--) {
		pos.x -= pos.w + (TileTx >> 2);
		SDL_BlitSurface(TileSet, &NumberSector[s[i] - '0'], Screen, &pos); // affiche les chiffres
	}
}


void FieldTransition(const uint8 base[16], const uint8 target[16]) { // anime la transition en fondu transparent du plateau `base` vers le plateau `target`
	SDL_Rect pos = {.w = TileS, .h = TileS};

	uint32 a = TileSet->format->Amask, time; // save le masque alpha du calque

	for (uint32 x = 0x07000007; !(x & 0x100); x += 0x08000008) {
		/* `a` vaut normalement 0xff000000 OU 0x000000ff (dépend du big/little endian)
		nous itérons donc sur ces 2 valeurs en même temps et nous filtrons avec `a` */

		time = SDL_GetTicks();

		for (uint8 i = 0; i < 16;  i++) if (base[i] != target[i]) {
			cellPos(pos, i);

			SDL_FillRect(Screen, &pos, SDL_MapRGB(Screen->format, 51, 51, 51));
			SDL_BlitSurface(TileSet, &TileSector[base[i]], Screen, &pos);
			TileSet->format->Amask = x & a; // on change le masque alpha par `x` filtré par `a`

			SDL_BlitSurface(TileSet, &TileSector[target[i]], Screen, &pos);
			TileSet->format->Amask = a;
		}

		SDL_Flip(Screen);
		if((time = AnimationFlash - SDL_GetTicks() + time) <= AnimationFlash) SDL_Delay(time);
	}
}


void EndAnimation(const bool victory, const bool revert){ // anime l'apparition (ou disparition si `revert`) du message "Partie Terminée" ou "Victoire"
	SDL_Rect pos = { (WIDTH - TextSector[victory ? 4: 3].w) / 2, Hud + (FramePadding - TextSector[victory ? 4: 3].h) / 2, TextSector[victory ? 4: 3].w, TextSector[victory ? 4: 3].h };
							
	uint32 a = TileSet->format->Amask, time;

	for (uint32 x = 0x07000007; !(x & 0x100); x += 0x08000008) {
		time = SDL_GetTicks(), TileSet->format->Amask = (revert ? ~x: x) & a;

		SDL_FillRect(Screen, &pos, SDL_MapRGB(Screen->format, 31, 31, 31));
		SDL_BlitSurface(TileSet, &TextSector[victory ? 4: 3], Screen, &pos);

		SDL_Flip(Screen);
		if((time = AnimationFlash - SDL_GetTicks() + time) <= AnimationFlash) SDL_Delay(time);
	}

	TileSet->format->Amask = a;
}


int8 getFieldState(const uint8 g[16]) {
	/* renvoie 1 si la partie est finie, càd si 16384 est atteint ou -1 si la partie est perdu, càd que aucun movement n'est possible,
	ou si le plateau est invalide, sinon 0 = la partie continue */

	bool m = 0;

	for (uint8 i = 0; i < 16; i++) {
		m |= g[i] > 0;
		if (g[i] > 13) return 1; // valeur limite (16384 = inf) du jeu atteinte -> partie terminée (victoire)
	}

	if (!m) return -1; // plateau vide = invalide -> partie terminée (évite un softlock)

	for (uint8 i = 0; i < 4; i++) for (uint8 u = 0; u < 4; u++) {
		if (!g[(i << 2) + u] || (i < 3 && g[(i << 2) + u] == g[((i + 1) << 2) + u]) || (u < 3 && g[(i << 2) + u] == g[(i << 2) + u + 1])) return 0; // un move est possible -> la partie continue
	}
	return -1; // sinon partie terminée (défaite)
}


bool RandintSpawn(const uint8 no, const uint8 value) { // ajoute `no` nombres sur le plateau (de valeur `1 << value` ou de valeur 2 ou 4 si `!value`), return 0 en cas d'impossibilité d'ajouter un nombre (ou si la partie est finie) sinon 1
	if (!no) return !getFieldState(GameField);

	uint8 u = 0, n;

	for (uint8 i = 0; i < 16; i++) u += (bool)GameField[i];

	if (u == 16) return 0; // toutes les cases sont remplies

	n = rand() % (16 - u); // index où mettre un nouveau nombre parmi les cases vides (càd ajoute un nouveau nombre a la n-ième case libre)

	for (u = 0; u < 16; u++) if (!GameField[u]) {
		if (!n) {
			GameField[u] = value ? value: rand() % 2 + 1;
			uint8 b[16];
			for (uint8 i = 0; i < 16; i++) b[i] = i == u ? 0: GameField[i];
			FieldTransition(b, GameField);
			return 1 && RandintSpawn(no - 1, value); // appel recursif, sortie par ce return garantie par définition de n
		}
		n--;
	}

	return 0; // impossible, est là pour eviter un warning
}


void ResetField() { // réinitialise le plateau et le score (= nouvelle partie)
	uint8 t[16] = {0};
	FieldTransition(GameField, t);

	for (uint8 i = 0; i < 16; i++) GameField[i] = 0;
	RandintSpawn(2, 1);
	Score = 0;
}


void BlitGrid(const bool fill) { // affiche le fond du plateau et des cellules vides si `fill == 1`
	SDL_Rect pos = {FramePadding, FramePadding + Hud, (TileS << 2) + Padding * 5, (TileS << 2) + Padding * 5};
	SDL_FillRect(Screen, &pos, SDL_MapRGB(Screen->format, 51, 51, 51));

	if (fill) {
		pos.w = pos.h = TileS;
		for (uint8 i = 0; i < 16;  i++) {
			cellPos(pos, i);
			SDL_BlitSurface(TileSet, &TileSector[0], Screen, &pos);
		}
	}
}


void BlitField() { // affiche les cellules du plateau
	SDL_Rect pos = {.w = TileS, .h = TileS};

	for (uint8 i = 0; i < 16;  i++) {
		cellPos(pos, i);
		SDL_BlitSurface(TileSet, &TileSector[GameField[i]], Screen, &pos);
	}
}


void save(const bool matrix) { // sauvegarde
	/* données en octet: HHHHSSSSMMMMMMMMMMMMMMMM
	H(4): HighScore, S(4): Score, M(16): GameField (S et M ne sont pas forcément présent) */

	FILE * f = fopen("savefile.txt", "rb+");

	if (!f && !(f = fopen("savefile.txt", "wb"))) return;

	fwrite(&HighScore, sizeof(HighScore), 1, f);

	if (matrix) {
		fwrite(&Score, sizeof(Score), 1, f);
		fwrite(GameField, sizeof(GameField[0]), 16, f);
	}

	fclose(f);
}


bool load(const bool anim) { // charge la sauvegarde et renvoie son intégrité
	FILE * f = fopen("savefile.txt", "rb");
	if (!f) return 0;

	bool s = 1; // intégrité du fichier de sauvegarde
	uint8 gf[16] = {0};
	uint32 hs = 0, sc = 0;

	s &= 1 == fread(&hs, sizeof(hs), 1, f);
	s &= 1 == fread(&sc, sizeof(sc), 1, f);
	s &= 16 == fread(gf, sizeof(gf[0]), 16, f);
	fclose(f);

	s &= !getFieldState(gf);

	if (hs > HighScore) HighScore = hs;

	if (anim && s) {
		Score = sc;
		int8 r = getFieldState(GameField);
		if (r) EndAnimation(r == 1, 1);
		if (Score > HighScore) HighScore = Score;
		FieldTransition(GameField, gf);
		memcpy(GameField, gf, sizeof(gf));
	}

	return s;
}


Move key2move(const SDLKey k){ // convertion d'event SDL en action de type Move
	switch (k) {
		case SDLK_RIGHT: return Right;
		case SDLK_LEFT: return Left;
		case SDLK_UP: return Up;
		case SDLK_DOWN: return Down;
		case SDLK_SPACE: return Save;
		case SDLK_RETURN: return Load;
		case SDLK_TAB: return New;
		default: return None;
	}
}


bool move(const Move k){ // calcul, effectue et anime les déplacements
	bool changed = 0; // indique si au moins une cellule a changée
	
	uint8 endField[16] = {0};   // déplacement final
	MoveData vector[16] = {0}; //  info de déplacement pour chaque cellules (cible ? bouge ? fusionne ?...)
	memcpy(endField, GameField, sizeof(GameField));

	switch (k){
		case Right:
			for (uint8 i = 0; i < 4; i++) for (uint8 u = 2; u < (uint8)-1; u--) if (endField[(i << 2) + u]) {
				uint8 o = u;
				while (o <= 2 && !endField[(i << 2) + o + 1]) o++;
				if (o <= 2 && endField[(i << 2) + o + 1] == endField[(i << 2) + u] && !vector[(i << 2) + o + 1].mergedTarget) {
					endField[(i << 2) + u] = 0, Score += 1 << ++endField[(i << 2) + ++o];
					changed = vector[(i << 2) + o].mergedTarget = 1;
				}
				else if (o != u) endField[(i << 2) + o] = endField[(i << 2) + u], endField[(i << 2) + u] = 0, changed = 1;
				vector[(i << 2) + u].moving = (vector[(i << 2) + u].vectorX = o - u);
			}
			break;
		case Up:
			for (uint8 i = 0; i < 4; i++) for (uint8 u = 1; u < 4; u++) if (endField[(u << 2) + i]) {
				uint8 o = u;
				while (o > 0 && !endField[((o - 1) << 2) + i]) o--;
				if (o > 0 && endField[((o - 1) << 2) + i] == endField[(u << 2) + i] && !vector[((o - 1) << 2) + i].mergedTarget) {
					endField[(u << 2) + i] = 0, Score += 1 << ++endField[(--o << 2) + i];
					changed = vector[(o << 2) + i].mergedTarget = 1;
				}
				else if (o != u) endField[(o << 2) + i] = endField[(u << 2) + i], endField[(u << 2) + i] = 0, changed = 1;
				vector[(u << 2) + i].moving = (vector[(u << 2) + i].vectorY = o - u);
			}
			break;
		case Left:
			for (uint8 i = 0; i < 4; i++) for (uint8 u = 1; u < 4; u++) if (endField[(i << 2) + u]) {
				uint8 o = u;
				while (o > 0 && !endField[(i << 2) + o - 1]) o--;
				if (o > 0 && endField[(i << 2) + o - 1] == endField[(i << 2) + u] && !vector[(i << 2) + o - 1].mergedTarget) {
					endField[(i << 2) + u] = 0, Score += 1 << ++endField[(i << 2) + --o];
					changed = vector[(i << 2) + o].mergedTarget = 1;
				}
				else if (o != u) endField[(i << 2) + o] = endField[(i << 2) + u], endField[(i << 2) + u] = 0, changed = 1;
				vector[(i << 2) + u].moving = (vector[(i << 2) + u].vectorX = o - u);
			}
			break;
		case Down:
			for (uint8 i = 0; i < 4; i++) for (uint8 u = 2; u < (uint8)-1; u--) if (endField[(u << 2) + i]) {
				uint8 o = u;
				while (o <= 2 && !endField[((o + 1) << 2) + i]) o++;
				if (o <= 2 && endField[((o + 1) << 2) + i] == endField[(u << 2) + i] && !vector[((o + 1) << 2) + i].mergedTarget) {
					endField[(u << 2) + i] = 0, Score += 1 << ++endField[(++o << 2) + i];
					changed = vector[(o << 2) + i].mergedTarget = 1;
				}
				else if (o != u) endField[(o << 2) + i] = endField[(u << 2) + i], endField[(u << 2) + i] = 0, changed = 1;
				vector[(u << 2) + i].moving = (vector[(u << 2) + i].vectorY = o - u);
			}
			break;
		case Save: { save(1); return 0; }
		case Load: { load(1); return 0; }
		case New:  { ResetField(); return 0; }
		case None: return 0;
	}

	if (HighScore < Score) HighScore = Score; // met a jour le meilleur score
	
	
	// GFX
	SDL_Rect pos = {.w = TileS, .h = TileS};
	MoveData md;
	uint32 time;

	for (uint8 x = 0; x <= TileS + Padding; x += 8){ // anime les déplacements grace aux infos dans `vector`
		time = SDL_GetTicks();
		BlitGrid(0);

		for (uint8 i = 0; i < 16;  i++) {
			cellPos(pos, i);
			SDL_BlitSurface(TileSet, &TileSector[vector[i].moving ? 0 : GameField[i]], Screen, &pos);
		}

		for (uint8 i = 0; i < 16;  i++) if ((md = vector[i]).moving) {
			pos.x = Padding * (1 + i % 4) + TileS * (i % 4) + md.vectorX * x + FramePadding;
			pos.y = Padding * (1 + i / 4) + TileS * (i / 4) + md.vectorY * x + FramePadding + Hud;

			SDL_BlitSurface(TileSet, &TileSector[GameField[i]], Screen, &pos);
		}
		SDL_Flip(Screen);
		if ((time = AnimationMove - SDL_GetTicks() + time) <= AnimationMove) SDL_Delay(time);
	}
	
	for (uint8 i = 0; i < 16; i++) GameField[i] = vector[i].mergedTarget ? endField[i] - 1: endField[i];
	FieldTransition(GameField, endField); // anime les fusions

	memcpy(GameField, endField, sizeof(GameField)); // finalise l'action

	return changed;
}


//////////////////////////////////////////////////////////////////////////////////////////////////


int main(int argc, char ** argv){
	srand(time(NULL));

	if (argc && argv[0][0]) { // change le dossier courant vers celui de l'executable lui-même, afin de trouver la sauvegarde savefile.txt
		uint32 i = strlen(argv[0]) - 1;
		while (i < (uint32)-1 && argv[0][i] != '/' && argv[0][i] != '\\') i--;

		if (i < (uint32)-1) {
			char c = argv[0][i];
			argv[0][i] = 0;   // split le nom du fichier
			chdir(argv[0]);  //  change le dossier courant
			argv[0][i] = c; //   restaure le nom du fichier
		}
	}
	
	if (SDL_Init(SDL_INIT_VIDEO) || !(Screen = SDL_SetVideoMode(WIDTH, HEIGHT, 32, SDL_HWSURFACE)) || \
	 !(TileSet = SDL_CreateRGBSurfaceFrom((void *)HardcodedTileSet.pixel_data, HardcodedTileSet.width, HardcodedTileSet.height,
	 									  HardcodedTileSet.bytes_per_pixel << 3, HardcodedTileSet.width * HardcodedTileSet.bytes_per_pixel,
										  0x000000ff, 0x0000ff00, 0x00ff0000, 0xff000000))) return 1; // un print ici serait inutile car SDL < 2.0 bloque stdout et stderr

	SDL_WM_SetCaption("2048 Game", NULL);
	
	SDL_FillRect(Screen, NULL, SDL_MapRGB(Screen->format, 31, 31, 31));
	
	load(0); // charge le meilleur score depuis la sauvegarde si dispo
	
	BlitScore();
	BlitGrid(1);
	ResetField();
	
	SDL_Event event, next_event;
	bool RUN = 1, play = 1, moved;

	while(RUN){
		SDL_WaitEvent(&event);

		switch(event.type){
			case SDL_QUIT: { RUN = 0; break; } // Quitte
			case SDL_KEYDOWN:
				if (event.key.keysym.sym == SDLK_ESCAPE || event.key.keysym.sym == SDLK_q) { RUN = 0; break; } // Quitte

				if (!play) switch (key2move(event.key.keysym.sym)) { // état de fin de partie
					case Load:
						play = load(1);
						break;
					case New:
						EndAnimation(getFieldState(GameField) == 1, 1);
						play = 1;
						break;
					default: break;
				}

				if (play) { // état de partie active
					moved = move(key2move(event.key.keysym.sym));
					BlitScore();
					SDL_Flip(Screen);

					if (!(play = !moved || RandintSpawn(1, 0))) EndAnimation(getFieldState(GameField) == 1, 0);
				}
				break;
			default: break;
		}

		/* Empêche l'accumulation d'events qui génère une latence entre les animations. Cependant ignorer tout les events ayant eux lieu
		lors d'une animation, en vidant la file d'events, réduit fortement la fluidité, nous nous limitons donc à garder, au maximum,
		seulement le dernier event de la file (de type appuie de touche) dans `event_next`, améliorant ainsi énormément l'expérience utilisateur. */

		next_event.type = SDL_NOEVENT;
		while (SDL_PollEvent(&event)) if (event.type == SDL_KEYDOWN) next_event = event; // vide la file d'event et garde le dernier event d'appuie de touche
		if (next_event.type == SDL_KEYDOWN) SDL_PushEvent(&next_event);     			//  remet le prochain event dans la file (donc au début)
	}

	save(0);

	SDL_Quit();
	return 0;
}
