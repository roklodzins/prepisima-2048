
  // Copyright © 2021 - Romain Klodzinski & Evahn Le Gal
 //  2048 Game - Terminal Version
//   Crossplatform: Windows (10 et 11) + Linux testé et garantie


// Pragma
#pragma GCC optimize("Ofast") // force gcc à activer toutes les optimisations


// Includes
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>

#ifdef _WIN32
#include <direct.h>
#define chdir _chdir
#else
#include <unistd.h>
#endif


// Typedef
typedef unsigned int  uint32;
typedef unsigned char uint8;

typedef char  int8;
typedef _Bool bool; // un enum marcherait aussi mais serait moins pratique pour certaine optimisation


// Macros
#define WHITE  0u
#define BLACK  30u
#define RED    31u
#define GREEN  32u
#define YELLOW 33u
#define INDIGO 34u
#define PURPLE 35u
#define BLUE   36u

#define L_WHITE  1u
#define L_BLACK  90u
#define L_RED    91u
#define L_GREEN  92u
#define L_YELLOW 93u
#define L_INDIGO 94u
#define L_PURPLE 95u
#define L_BLUE   96u


#define print(...) printf(__VA_ARGS__) // print better than printf  (python)
#define set_color(color) print("\033[%um", (uint8) color) // change la couleur du terminal avec les Macros de couleurs


#ifdef _WIN32 // fonction de clear du terminal cross-plateforme
#define clear() system("cls") // windows
#else
#define clear() system("clear") // autres systèmes inutiles comme linux
#endif


// Enum
typedef enum Move {
	Right = 0u,
	Up = 1u,
	Left = 2u,
	Down = 3u,
	Save = 4u,
	Load = 5u,
} Move; 



// Global Variables
uint8 GameField[16]; // on stock ici les exposants des nombres et non leur valeur
uint32 Score = 0, HighScore = 0;



void cprint(const uint8 color, const char * const str, ...) { // comme print mais change la couleur du terminal pour l'écriture
	va_list arg;
	va_start(arg, str);
	set_color(color);
	vprintf(str, arg);
	va_end(arg);
	set_color(WHITE);
}


int input() { // alternative à scanf
	char c;
	do { c = getchar(); } while (c == '\n' || c == EOF);
	return c;
}


void wait_enter() { // pause la console et attend une entrée pour continuer
	cprint(L_BLACK, "Appuyer sur Entrée pour continuer...\n");
	getchar();
	getchar();
}


int8 getFieldState(const uint8 g[16]) {
	/* renvoie 1 si la partie est finie, càd si 16384 est atteint ou -1 si la partie est perdu, càd que aucun movement n'est possible,
	ou si le plateau est invalide, sinon 0 = la partie continue */

	bool m = 0;

	for (uint8 i = 0; i < 16; i++) {
		m |= g[i] > 0;
		if (g[i] > 13) return 1; // valeur limite (16384 = inf) du jeu atteinte -> partie terminée (victoire)
	}

	if (!m) return -1; // plateau vide = invalide -> partie terminée (évite un softlock)

	for (uint8 i = 0; i < 4; i++) for (uint8 u = 0; u < 4; u++) {
		if (!g[(i << 2) + u] || (i < 3 && g[(i << 2) + u] == g[((i + 1) << 2) + u]) || (u < 3 && g[(i << 2) + u] == g[(i << 2) + u + 1])) return 0; // un move est possible -> la partie continue
	}
	return -1; // sinon partie terminée (défaite)
}


bool RandintSpawn(const uint8 no, const uint8 value) { // ajoute `no` nombres sur le plateau (de valeur `1 << value` ou de valeur 2 ou 4 si `!value`), return 0 en cas d'impossibilité d'ajouter un nombre (ou si la partie est finie) sinon 1
	if (!no) return !getFieldState(GameField);

	uint8 u = 0, n;

	for (uint8 i = 0; i < 16; i++) u += (bool)GameField[i];

	if (u == 16) return 0; // toutes les cases sont remplies

	n = rand() % (16 - u); // index où mettre un nouveau nombre parmi les cases vides (càd ajoute un nouveau nombre a la n-ième case libre)

	for (u = 0; u < 16; u++) if (!GameField[u]) {
		if (!n) {
			GameField[u] = value ? value: rand() % 2 + 1;
			return 1 && RandintSpawn(no - 1, value); // appel recursif, sortie par ce return garantie par définition de n
		}
		n--;
	}

	return 0; // impossible, est là pour eviter un warning
}


void ResetField() { // réinitialise le plateau et le score (= nouvelle partie)
	for (uint8 i = 0; i < 16; i++) GameField[i] = 0;
	RandintSpawn(2, 1);
	Score = 0;
}


void save(const bool matrix) { // sauvegarde
	/* données en octet: HHHHSSSSMMMMMMMMMMMMMMMM
	H(4): HighScore, S(4): Score, M(16): GameField (S et M ne sont pas forcément présent) */
	
	FILE * f = fopen("savefile.txt", "rb+");

	if (!f && !(f = fopen("savefile.txt", "wb"))) return;

	fwrite(&HighScore, sizeof(HighScore), 1, f);

	if (matrix) {
		fwrite(&Score, sizeof(Score), 1, f);
		fwrite(GameField, sizeof(GameField[0]), 16, f);
	}

	fclose(f);
}


bool load(const bool v) { // charge la sauvegarde et renvoie son intégrité
	FILE * f = fopen("savefile.txt", "rb");
	if (!f) return 0;

	bool s = 1;
	uint8 gf[16] = {0};
	uint32 hs = 0, sc = 0;

	s &= 1 == fread(&hs, sizeof(hs), 1, f);
	s &= 1 == fread(&sc, sizeof(sc), 1, f);
	s &= 16 == fread(gf, sizeof(gf[0]), 16, f);
	fclose(f);

	s &= !getFieldState(gf);

	if (hs > HighScore) HighScore = hs;

	if (s) {
		Score = sc;
		memcpy(GameField, gf, 16 * sizeof(gf[0]));
	}

	if (v) {
		if (s) cprint(L_GREEN, "Partie chargée !\n");
		else cprint(L_RED, "Partie non chargée : sauvegarde invalide, corrompue ou inexistante\n");
		wait_enter();
	}

	return s;
}


bool move(const Move k){ // calcul et effectue les déplacements
	bool changed = 0, merged[16] = {0}; // changed: indique si au moins une cellule a changée,    merged: indique si la case `i` est cible d'une fusion

	switch (k){
		case Right:
			for (uint8 i = 0; i < 4; i++) for (uint8 u = 2; u < (uint8)-1; u--) if (GameField[(i << 2) + u]) {
				uint8 o = u;
				while (o <= 2 && !GameField[(i << 2) + o + 1]) o++;
				if (o <= 2 && GameField[(i << 2) + o + 1] == GameField[(i << 2) + u] && !merged[(i << 2) + o + 1]) {
					GameField[(i << 2) + u] = 0, Score += 1 << ++GameField[(i << 2) + ++o];
					changed = merged[(i << 2) + o] = 1;
				}
				else if (o != u) GameField[(i << 2) + o] = GameField[(i << 2) + u], GameField[(i << 2) + u] = 0, changed = 1;
			}
			break;
		case Up:
			for (uint8 i = 0; i < 4; i++) for (uint8 u = 1; u < 4; u++) if (GameField[(u << 2) + i]) {
				uint8 o = u;
				while (o > 0 && !GameField[((o - 1) << 2) + i]) o--;
				if (o > 0 && GameField[((o - 1) << 2) + i] == GameField[(u << 2) + i] && !merged[((o - 1) << 2) + i]) {
					GameField[(u << 2) + i] = 0, Score += 1 << ++GameField[(--o << 2) + i];
					changed = merged[(o << 2) + i] = 1;
				}
				else if (o != u) GameField[(o << 2) + i] = GameField[(u << 2) + i], GameField[(u << 2) + i] = 0, changed = 1;
			}
			break;
		case Left:
			for (uint8 i = 0; i < 4; i++) for (uint8 u = 1; u < 4; u++) if (GameField[(i << 2) + u]) {
				uint8 o = u;
				while (o > 0 && !GameField[(i << 2) + o - 1]) o--;
				if (o > 0 && GameField[(i << 2) + o - 1] == GameField[(i << 2) + u] && !merged[(i << 2) + o - 1]) {
					GameField[(i << 2) + u] = 0, Score += 1 << ++GameField[(i << 2) + --o];
					changed = merged[(i << 2) + o] = 1;
				}
				else if (o != u) GameField[(i << 2) + o] = GameField[(i << 2) + u], GameField[(i << 2) + u] = 0, changed = 1;
			}
			break;
		case Down:
			for (uint8 i = 0; i < 4; i++) for (uint8 u = 2; u < (uint8)-1; u--) if (GameField[(u << 2) + i]) {
				uint8 o = u;
				while (o <= 2 && !GameField[((o + 1) << 2) + i]) o++;
				if (o <= 2 && GameField[((o + 1) << 2) + i] == GameField[(u << 2) + i] && !merged[((o + 1) << 2) + i]) {
					GameField[(u << 2) + i] = 0, Score += 1 << ++GameField[(++o << 2) + i];
					changed = merged[(o << 2) + i] = 1;
				}
				else if (o != u) GameField[(o << 2) + i] = GameField[(u << 2) + i], GameField[(u << 2) + i] = 0, changed = 1;
			}
			break;
		case Save: { save(1); break; }
		case Load: { load(1); break; }
	}

	if (HighScore < Score) HighScore = Score;

	return changed;
}


void printField(){ // affiche le score et le plateau en couleur et de facon jolie grace aux caractères Unicode
	clear();

	print("Score: %u\tMeilleur Score: %u\n\n", Score, HighScore);
	cprint(L_BLACK, "\u250c\u2500\u2500\u2500\u2500\u2500\u2500\u252c\u2500\u2500\u2500\u2500\u2500\u2500\u252c\u2500\u2500\u2500\u2500\u2500\u2500\u252c\u2500\u2500\u2500\u2500\u2500\u2500\u2510\n\u2502");

	for (uint8 i = 0; i < 16; i++) {
		if (i / 4 && !(i % 4)) cprint(L_BLACK, "\n\u251c\u2500\u2500\u2500\u2500\u2500\u2500\u253c\u2500\u2500\u2500\u2500\u2500\u2500\u253c\u2500\u2500\u2500\u2500\u2500\u2500\u253c\u2500\u2500\u2500\u2500\u2500\u2500\u2524\n\u2502");
		switch (GameField[i]) {
			case 1: { cprint(L_YELLOW, "   2  "); break; }
			case 2: { cprint(L_GREEN,  "   4  "); break; }
			case 3: { cprint(L_BLUE,   "   8  "); break; }
			case 4: { cprint(L_INDIGO, "  16  "); break; }
			case 5: { cprint(L_PURPLE, "  32  "); break; }
			case 6: { cprint(L_RED,    "  64  "); break; }
			case 7: { cprint(L_WHITE,  "  128 "); break; }
			case 8: { cprint(YELLOW,   "  256 "); break; }
			case 9: { cprint(GREEN,    "  512 "); break; }
			case 10: { cprint(BLUE,    " 1024 "); break; }
			case 11: { cprint(INDIGO,  " 2048 "); break; }
			case 12: { cprint(PURPLE,  " 4096 "); break; }
			case 13: { cprint(RED,     " 8192 "); break; }
			case 14: { cprint(WHITE,   "   \u221e  "); break; } // infini
			default: { print("      "); break; }
		}
		cprint(L_BLACK, "\u2502");
	}

	cprint(L_BLACK, "\n\u2514\u2500\u2500\u2500\u2500\u2500\u2500\u2534\u2500\u2500\u2500\u2500\u2500\u2500\u2534\u2500\u2500\u2500\u2500\u2500\u2500\u2534\u2500\u2500\u2500\u2500\u2500\u2500\u2518\n");

	print("\n");
}


Move ask() {
	print("g: gauche   d: droite   h: haut   b: bas (ou zqsd)\ne: sauvegarder   c: charger   x: quitter\n:");

	switch (input()){
		case 'q':
		case 'g': return Left;
		case 'd': return Right;
		case 'z':
		case 'h': return Up;
		case 's':
		case 'b': return Down;
		case 'e': return Save;
		case 'c': return Load;
		case 'x': exit(0);
	}
	return ask();
}


//////////////////////////////////////////////////////////////////////////////////////////////////


int main(int argc, char ** argv){
	srand(time(NULL));

	#ifdef _WIN32
	system("chcp 65001"); // active (force) l'unicode sur Cmd/Windows
	#endif

	if (argc && argv[0][0]) { // change le dossier courant vers celui de l'executable lui-même, afin de trouver la sauvegarde savefile.txt
		uint32 i = strlen(argv[0]) - 1;
		while (i < (uint32)-1 && argv[0][i] != '/' && argv[0][i] != '\\') i--;
		
		if (i < (uint32)-1) {
			char c = argv[0][i];
			argv[0][i] = 0;   // split le nom du fichier
			chdir(argv[0]);  //  change le dossier courant
			argv[0][i] = c; //   restaure le nom du fichier
		}
	}
	
	char c = 0;
	
	while (c != 'x') {
		load(0);

		clear();
		print("Copyright © 2021 - Romain Klodzinski & Evahn Le Gal\nMenu Principal - 2048 Game\n\nMeilleur Score: %u\n\nn: nouvelle partie\nr: reprendre une partie interrompue\nx: quitter\n\nChoix: ", HighScore);
		if ((c = input()) == 'x') break;

		switch (c){
			case 'n':
				ResetField();
				break;

			case 'r':
				clear();
				if (!load(1)) continue;
				break;
			
			default: continue;
		}

		do printField(); while (!move(ask()) || RandintSpawn(1, 0));

		c = 0;
		for (uint8 i = 0; i < 16; i++) if (GameField[i] > 13) c = 1;

		printField();

		if (c) cprint(L_GREEN, "Bravo, vous avez gagnez !\n\n");
		else cprint(L_BLUE, "Partie terminée\n\n");
		save(0);
		wait_enter();
	}

	return 0;
}
