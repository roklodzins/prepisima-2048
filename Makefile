CC=gcc
CFLAGS= -Wall -Wextra -pedantic

SOURCE="main - terminal.c"
SDL_SOURCE=main.c

ifeq ($(OS),Windows_NT) # -mwindows = windowed app
    CSDL= -Ic:\mingw64\local\include\SDL -D_GNU_SOURCE=1 -Dmain=SDL_main -Lc:\mingw64\local\lib -lmingw32 -lSDLmain -lSDL -mwindows
    SDL_EXECUTABLE=main.exe
    EXECUTABLE="main - terminal.exe"
else
    CSDL= `sdl-config --cflags --libs`
    SDL_EXECUTABLE=main.elf
    EXECUTABLE="main - terminal.elf"
endif

all:
	$(CC) $(SDL_SOURCE) -o $(SDL_EXECUTABLE) $(CFLAGS) $(CSDL)
	$(CC) $(SOURCE) -o $(EXECUTABLE) $(CFLAGS)