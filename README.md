# PrepISIMA 2048

## Description

This project is a 2048 Game,
done as a Prep'ISIMA 2nd year project

## Details

The game exists in 2 versions:
- A Terminal version ![Terminal](./doc/term.png "Terminal")
- And a SDL 1.2 version ![SDL 1.2](./doc/sdl.png "SDL 1.2")

For more information, see doc/

## Build

A makefile is provided but the SDL 1.2 version may not compile easily
Thus, the SDL 1.2 version is provided in exe.zip (Windows build)


